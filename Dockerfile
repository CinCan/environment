ARG UBUNTU_VERSION=bionic
ARG CONCOURSE_VERSION=6.0.0

FROM ubuntu:$UBUNTU_VERSION as builder

LABEL MAINTAINER=cincan.io

ENV DEBIAN_FRONTEND noninteractive
ARG WGET_OPTIONS="-q --show-progress --progress=bar:force:noscroll"
ARG CONCOURSE_VERSION

RUN apt-get update && apt-get install -y \
	tar \
	wget \
	perl &&\
	rm -rf /var/lib/apt/lists/* &&\
	mkdir -p /assets &&\
	wget $WGET_OPTIONS -O /assets/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 &&\
	chmod +x /assets/dumb-init &&\
	wget $WGET_OPTIONS -O concourse_linux_amd64.tgz https://github.com/concourse/concourse/releases/download/v"${CONCOURSE_VERSION}"/concourse-"${CONCOURSE_VERSION}"-linux-amd64.tgz &&\
	tar -xzvf /concourse_linux_amd64.tgz -C /usr/local &&\
	chmod +x /usr/local/concourse/bin/concourse &&\
	tar -xzvf /usr/local/concourse/fly-assets/fly-linux-amd64.tgz -C /usr/local/concourse/bin &&\
	chmod +x /usr/local/concourse/bin/fly

FROM ubuntu:$UBUNTU_VERSION as deploy

LABEL MAINTAINER=cincan.io

COPY --from=builder /assets /usr/local/bin
COPY --from=builder /usr/local /usr/local

ENV DEBIAN_FRONTEND=noninteractive
ENV PATH="/usr/local/concourse/bin:${PATH}"

ADD ./bootstrap-node.sh .

RUN apt-get update && apt-get install -y \
	btrfs-tools \
        ca-certificates \
        git \
        iproute2 \
        iputils-ping \
	file &&\
	rm -rf /var/lib/apt/lists/* &&\
	chmod +x ./bootstrap-node.sh

EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/dumb-init", "/bootstrap-node.sh"]
