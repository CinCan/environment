#!/bin/sh

CONCOURSE_VERSION=$(concourse -v)

if [ "$HOSTNAME" = "concourse.cincan.io" ]; then
	if [ $CONCOURSE_VERSION = 4.2.2 ]; then
	        ssh-keygen -o -t rsa -m pem -b 4096 -q -N '' -f "${KEYS_PATH}/tsa_host_key"
	        ssh-keygen -o -t rsa -m pem -b 4096 -q -N '' -f "${KEYS_PATH}/worker_key"
	        ssh-keygen -o -t rsa -m pem -b 4096 -q -N '' -f "${KEYS_PATH}/session_signing_key"
	        ssh-keygen -o -t rsa -m pem -b 4096 -q -N '' -f "${KEYS_PATH}/cincan_rsa"
	        cp "${KEYS_PATH}/worker_key.pub" "${KEYS_PATH}/authorized_worker_keys"
		concourse web
	else
		concourse generate-key -t rsa -f "${KEYS_PATH}/session_signing_key"
		concourse generate-key -t ssh -f "${KEYS_PATH}/tsa_host_key"
		concourse generate-key -t ssh -f "${KEYS_PATH}/worker_key"
	        ssh-keygen -o -t rsa -m pem -b 4096 -q -N '' -f "${KEYS_PATH}/cincan_rsa"
		cp "${KEYS_PATH}/worker_key.pub" "${KEYS_PATH}/authorized_worker_keys"
		concourse web
	fi
else
	echo "Waiting for the web node to bootstrap"
	sleep 60
	concourse worker
fi

