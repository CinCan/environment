#!/bin/bash
#
# CI test & build script for https://gitlab.com/CinCan/environment/ - if you're an end user, you don't need to worry about this.

# Remove previous installation files
ssh "$SSH_REMOTE_SERVER" "rm -rf $REMOTE_FOLDER && mkdir $REMOTE_FOLDER"

# Copy repo files to remote server
scp -r ./* "$SSH_REMOTE_SERVER":"$REMOTE_FOLDER"

# Set up the pilot environment through SSH, test it, and push new version of concourse-ci to Docker Hub
yes | head -n 1 | ssh -t "$SSH_REMOTE_SERVER" "cd /opt/cincan/build &&
	docker stop \$(docker ps -a -q) || echo 'No running containers' && docker-compose down --rmi all &&
	docker system prune -a --volumes &&
	C_CI_TAG=\$(curl -Ss 'https://registry.hub.docker.com/v2/repositories/cincan/c-ci/tags/?page_size=100'| \
		python -c 'import sys,json;data=json.load(sys.stdin);n=\"\\n\";print (n.join([x[\"name\"] for x in data[\"results\"]]))'| \
		sort --version-sort --reverse |grep -v -e test -e dev | sed -n 1p) &&
        CONCOURSE_TAG=\$(curl -Ss 'https://registry.hub.docker.com/v2/repositories/concourse/concourse/tags/?page_size=100' | jq '.results[].name' | tr -d 'a-zA-Z\-{\" ' |sort | tail -n 1) &&
        /bin/echo 'Current cincan/c-ci image version is '\$C_CI_TAG &&	/bin/echo 'Current cincan/c-ci image version is '\$C_CI_TAG &&
	if [ \$(echo \$C_CI_TAG |tr -d .) -lt \$(echo \$CONCOURSE_TAG |tr -d .) ]; then
		TAG=\$CONCOURSE_TAG
		/bin/echo -e 'Concourse has a newer version, updating to '\$TAG
	else
		TAG=\$C_CI_TAG
	fi &&
	cd $REMOTE_FOLDER &&
	docker build . -t cincan/c-ci:\$TAG -t cincan/c-ci:latest --build-arg CONCOURSE_VERSION=\$TAG &&
	docker images"

# Steps:
#	Clear old images
#	Compare c-ci tag and latest Concourse tag. Update if new Concourse version is available.
# 	Build newest concourse-ci image from the Dockerfile
