#!/bin/bash
#
# CI test & build script for https://gitlab.com/CinCan/environment/ - if you're an end user, you don't need to worry about this
PIPELINE="pdf-pipeline"

# Set up the pilot environment through SSH, test it, and push new version of concourse-ci to Docker Hub
yes | head -n 1 | ssh -t "$SSH_REMOTE_SERVER" "cd /opt/cincan/build &&
	/bin/echo 'setting up pipeline: ' $PIPELINE &&
	sudo bash ./setup-pipeline.sh pdf-pipeline &&
	echo 'Checking that pipeline is up' &&
	/bin/echo 'Check login' &&
	docker exec concourse.cincan.io fly -t local status &&
	/bin/echo 'Check resources' &&
	docker exec concourse.cincan.io fly -t local check-resource -r $PIPELINE/sample-source &&
	docker exec concourse.cincan.io fly -t local check-resource -r $PIPELINE/results &&
	docker exec concourse.cincan.io fly -t local unpause-pipeline -p $PIPELINE &&

	job_number=1 &&
	jobs=\$(docker exec concourse.cincan.io fly -t local jobs --pipeline $PIPELINE |awk NR==\$job_number) &&

	/bin/echo 'Waiting for the pipeline to start...' &&
	for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
		do
		if [ $(/bin/echo '\$jobs' |sed -n '/start/p' |tr -d ' ') -o $(/bin/echo '\$jobs' |sed -n '/succeed/p' |tr -d ' ') ]; then break; fi

		if [ \$i -eq 15 ]; then
			/bin/echo 'Pipeline did not start, exiting'
			exit 1
		fi
		sleep 5;
	done &&

	/bin/echo 'Pipeline started, checking jobs' &&
        i=1 &&
        while
		jobschanged=\$(docker exec concourse.cincan.io fly -t local jobs --pipeline $PIPELINE |awk NR==\$job_number)

		if [ \$(/bin/echo \$jobs |tr -d '/ ') != \$(/bin/echo \$jobschanged |tr -d '/ ') ]; then
			/bin/echo -e '\nNext job status:' \$jobschanged '\n'
		fi

		jobs=\$(docker exec concourse.cincan.io fly -t local jobs --pipeline $PIPELINE |awk NR==\$job_number)
                job_status=\$(/bin/echo \$jobs |awk '{print \$3}')

		if [ \$job_status = 'succeeded' ]; then
			/bin/echo -e '\nJob succeeded:' $jobs \$(echo \$jobs |awk '{print \$1}') '\n'
			if [ \$job_number -eq 5 ]; then
				/bin/echo 'Environment succesfully tested! Pushing to Docker hub.'
				docker push cincan/c-ci
				exit 0
			else
				i=1
				job_number=\$((job_number+1))
			fi
		elif [ \$job_status = 'failed' ]; then
			break
		fi
	    	sleep 10
	    	/bin/echo -en '.'

		: \${start=\$i}
		i=\$((i+1))
		[ \$i -lt 20 ]
	do :;done &&
	/bin/echo 'Pipeline test has failed...' &&
	exit 1"

# Steps:
# 	Set up $PIPELINE for testing:
# 	Check login
# 	Check pipeline resources
# 	Check that the pipeline starts
# 	Check that all jobs succeed
# 	Push to Docker Hub cincan/c-ci:latest and cincan/c-ci:TAG
