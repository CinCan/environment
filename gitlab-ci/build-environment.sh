#!/bin/bash
#
# CI test & build script for https://gitlab.com/CinCan/environment/ - if you're an end user, you don't need to worry about this.

# Set up the pilot environment through SSH, test it, and push new version of concourse-ci to Docker Hub
yes | head -n 1 | ssh -t "$SSH_REMOTE_SERVER" "cd $REMOTE_FOLDER &&
	sudo rm -rf /opt/cincan &&
	/bin/echo '$DOCKERHUB_PASS' | docker login -u '$DOCKERHUB_USER' --password-stdin  >/dev/null 2>/dev/null &&
	sudo ./build.sh &&
	export CI=latest &&
	cd /opt/cincan/build &&
	docker-compose up -d --remove-orphans ci worker db gitlab &&
	docker ps &&
	/bin/echo -e '\n\nconcourse, worker, gitlab, db status:' &&
	docker inspect -f '{{ .State.Status}}' concourse.cincan.io worker.cincan.io gitlab.cincan.io db.cincan.io &&
	for i in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15; do
		health=\$(docker inspect -f '{{ .State.Health.Status}}' gitlab.cincan.io)
		sleep 30
		/bin/echo 'waiting for Gitlab to get up... Status: '\$health
		running_conts=\$(docker inspect -f '{{ .State.Status}}' concourse.cincan.io worker.cincan.io gitlab.cincan.io db.cincan.io | grep -c 'running')
		/bin/echo \$running_conts'/4 containers running'
		if [ \$running_conts -lt 4 ]; then
			/bin/echo 'trying again...'
			docker-compose up -d --remove-orphans ci worker db gitlab
		fi
		if [ \$health = 'healthy' ] && [ \$running_conts -ge 4 ]; then
			/bin/echo 'Gitlab is up'
			/bin/echo -e '\n\nconcourse, worker, gitlab, db status:'
			docker inspect -f '{{ .State.Status}}' concourse.cincan.io worker.cincan.io gitlab.cincan.io db.cincan.io
			break
		else
			/bin/echo -e '\n\nconcourse, worker, gitlab, db status:'
			docker inspect -f '{{ .State.Status}}' concourse.cincan.io worker.cincan.io gitlab.cincan.io db.cincan.io
		fi
	done &&
	docker ps &&
	echo \$running_conts \$health &&
	if [ \$running_conts -ge 4 ] && [ \$health = 'healthy' ]; then
		/bin/echo 'Environment set up OK. Starting pipeline setup...'
	else
		/bin/echo 'Failed'
		/bin/echo -e '\n\nconcourse, worker, gitlab, db status:'
		docker inspect -f '{{ .State.Status}}' concourse.cincan.io worker.cincan.io gitlab.cincan.io db.cincan.io
		exit 1
	fi"

# Steps:
# 	Run build.sh
# 	Run docker-compose up -d ci gitlab db worker
# 	Inspect all 4 containers, build fails, if not all 4 are running
# 	Wait for Gitlab to get up, fail if not healthy after number of epochs
