#! /bin/bash -e

# Welcome banner

echo "    _______       ______           "
echo "  / ____(_)___  / ____/___ _____   "
echo " / /   / / __ \/ /   / __ \`/ __ \ "
echo "/ /___/ / / / / /___/ /_/ / / / /  "
echo "\____/_/_/ /_/\____/\__,_/_/ /_/   "
echo ""
echo "Continuous Integration for the Collaborative Analysis of Incidents"
echo ""

# Variable definitions

CHECK="\e[32m[+]\e[0m"
WARNING="\e[33m[!]\e[0m"
ERROR="\e[0;91m[x]\e[0m"

# note: providing custom values for these only works if you also supply
# a TLS cert. This script always generates self-signed certs for the below defaults.

: "${COMMON_NAME_CONCOURSE:=concourse.cincan.io}"
: "${COMMON_NAME_GITLAB:=gitlab.cincan.io}"
: "${EXTERNAL_URL_CONCOURSE:=172.20.0.3}"
: "${ALT_EXTERNAL_URL_CONCOURSE:=concourse.cincan.io}"
: "${EXTERNAL_URL_GITLAB:=172.20.0.5}"
: "${ALT_EXTERNAL_URL_GITLAB:=gitlab.cincan.io}"
: "${BUILD_PATH:=/opt/cincan}"
: "${CONCOURSE_VERSION:=6.0.0}"
: "${GITLAB_VERSION:=12.2.4-ce.0}"
: "${COMMON_NAME_REGISTRY:=registry.cincan.io}"
: "${EXTERNAL_URL_REGISTRY:=172.20.0.6}"
: "${ALT_EXTERNAL_URL_REGISTRY:=registry.cincan.io}"

# Function definitions

help (){
	echo ""
	echo "Usage:"
	echo ""
	echo "-s str       - DNS server [default: 8.8.8.8]"
	echo "-u str       - username [default: cincan]"
	echo "-p str       - password"
	echo "-d file      - database name [default: cincan]"
	echo "-v version   - Concourse version to be used [default: $CONCOURSE_VERSION]"
	echo "-e var=value - optional variables (can be set multiple times):"
	echo ""
	echo "COMMON_NAME_CONCOURSE      =    Common Name used for Concourse self-signed certificate [default: concourse.cincan.io]"
	echo "COMMON_NAME_GITLAB         =    Common Name used for Gitlab self-signed certificate [default: gitlab.cincan.io]"
	echo "EXTERNAL_URL_CONCOURSE     =    External URL used for Concourse [default: 172.20.0.3]"
	echo "ALT_EXTERNAL_URL_CONCOURSE =    Alternative URL used for Concourse [default: concourse.cincan.io]"
	echo "EXTERNAL_URL_GITLAB        =    External URL used for Gitlab [default: 172.20.0.5]"
	echo "ALT_EXTERNAL_URL_GITLAB    =    Alternative URL used for Gitlab [default: gitlab.cincan.io]"
	echo "EXTERNAL_URL_REGISTRY      =    External URL used for Docker private registry [default: 172.20.0.6]"
	echo "ALT_EXTERNAL_URL_REGISTRY  =    Alternative URL used for Docker private registry [default: registry.cincan.io]"
	echo "BUILD_PATH                 =    Path whereto CinCan environment configuration files will be generated [default: /opt/cincan]"
	echo ""
	exit 0
}

discover_dns_server (){
	if which systemd-resolve >/dev/null 2>&1; then
		systemd-resolve --status | sed -n 's/.*DNS Servers: \(.*\)/\1/p'
	else
		grep -E '^nameserver\b' /etc/resolv.conf | grep -Fv 127.0.0 | head -1 | awk '{print $2}'
	fi
}

# Check if the user is root, exit if not

if [ ! "${UID}" -eq 0 ]; then
        echo -e "$ERROR Please run the script as root"; exit 0
fi

# Command-line arguments parsing

while getopts :s:u:p:d:v:e: option; do
	case "${option}" in
		s)
			DNS=${OPTARG};;
		u)
			USER_NAME=${OPTARG};;
		p)
			PASSWORD=${OPTARG};;
		d)
			DATABASE=${OPTARG};;
		v)
			CONCOURSE_VERSION=${OPTARG};;
		e)
			ENVS+=(${OPTARG});;
		\?)
			help;;
		:)
			echo "Option -${OPTARG} requires a parameter."
			help;;
	esac
done

# Loop trough -e option optional variable assignments and set them to corresponsive variables

for val in "${ENVS[@]}"; do
	if [[ ${val%=*} =~ ^(COMMON_NAME_CONCOURSE|common_name_concourse)$ ]]; then
		COMMON_NAME_CONCOURSE=${val##*=}
	fi
	if [[ ${val%=*} =~ ^(COMMON_NAME_GITLAB|common_name_gitlab)$ ]]; then
		COMMON_NAME_GITLAB=${val##*=}
	fi
	if [[ ${val%=*} =~ ^(EXTERNAL_URL_CONCOURSE|external_url_concourse)$ ]]; then
		EXTERNAL_URL_CONCOURSE=${val##*=}
	fi
	if [[ ${val%=*} =~ ^(ALT_EXTERNAL_URL_CONCOURSE|alt_external_url_concourse)$ ]]; then
		ALT_EXTERNAL_URL_CONCOURSE=${val##*=}
	fi
	if [[ ${val%=*} =~ ^(EXTERNAL_URL_GITLAB|external_url_gitlab)$ ]]; then
		EXTERNAL_URL_GITLAB=${val##*=}
	fi
	if [[ ${val%=*} =~ ^(ALT_EXTERNAL_URL_GITLAB|alt_external_url_gitlab)$ ]]; then
		ALT_EXTERNAL_URL_GITLAB=${val##*=}
	fi
	if [[ ${val%=*} =~ ^(EXTERNAL_URL_REGISTRY|external_url_registry)$ ]]; then
		EXTERNAL_URL_REGISTRY=${val##*=}
	fi
	if [[ ${val%=*} =~ ^(ALT_EXTERNAL_URL_REGISTRY|alt_external_url_registry)$ ]]; then
		ALT_EXTERNAL_URL_REGISTRY=${val##*=}
	fi
	if [[ ${val%=*} =~ ^(BUILD_PATH|build_path)$ ]]; then
		BUILD_PATH=${val##*=}
	fi
done

# Username, password, DNS and database definitions

if [[ -z "$USER_NAME" || -z "$PASSWORD" || -z "$DNS" || -z "$DATABASE" ]]; then
	if [ -z "$USER_NAME" ]; then
        echo -e "$CHECK No user input for username. Using default: ${USER_NAME:=cincan}"
	fi
	if [ -z "$PASSWORD" ]; then
		echo -e "$CHECK No user input for password. Using generated: ${PASSWORD:=$(head -c 9 /dev/urandom | openssl base64 -e)}"
	fi
	if [ -z "$DNS" ]; then
		DNS=$(discover_dns_server)
		if [ -z "$DNS" ]; then
			echo "Couldn't autodetect DNS server and no DNS server specified"
			exit 1
		fi

		if [ ! $(echo "$DNS" | wc -l) -eq 1 ]; then
			echo -e "$WARNING There are multiple DNS servers available, please choose the one to use:"
			i=0
			PS3="Select DNS server: "
			while [ $i -lt $(echo "$DNS" |wc -l) ]; do
			        i=$((i+1))
			        options[i]=$(echo "$DNS" |awk NR==$i)
			done
			options[$((i+1))]="Quit"

			select CHOOSE_DNS in "${options[@]}"; do
			        if [[ -n $CHOOSE_DNS ]]; then
			                DNS=$CHOOSE_DNS
		        	        break
			        else
					if [ "$REPLY" = "q" ] || [ "$REPLY" = "Q" ]; then exit; fi
			                echo -e "$ERROR invalid option"
			        fi
			done
		else
			echo -e "$CHECK No user input for DNS. Using host's first reported DNS server: ${DNS}"
		fi
	fi
	if [ -z "$DATABASE" ]; then
		echo -e "$CHECK No user input for database. Using default: ${DATABASE:=cincan}"
	fi
fi

# Check if ${BUILD_PATH}/concourse directory exist and delete it

if [ -d "${BUILD_PATH}/concourse" ]; then
	echo -e "$WARNING Directory ${BUILD_PATH}/concourse already exists... removing"
	rm -rf "${BUILD_PATH}/concourse"
fi

# Check if ${BUILD_PATH}/build directory exists, if yes stop and delete old containers

if [ -d "${BUILD_PATH}/build" ]; then
	echo -e "$WARNING Directory ${BUILD_PATH}/build exists... stopping and removing old containers"
	WORKDIR=$(pwd)
	cd "${BUILD_PATH}/build"
	if [[ $(docker ps |grep add2git) ]]; then docker stop add2git.cincan.io;fi
	docker-compose down --volumes --rmi all
	cd "${WORKDIR}"
fi

# Create directories if not exists

echo -e "$CHECK Creating directories"
mkdir -p "${BUILD_PATH}"/{certs,keys,build,data,pipelines,gitlab/config}

# Create config.json for the dns-proxy, even though the user do not necessarily use the dns-proxy

echo -e "$CHECK Creating config.json"
if [[ $DNS != 8.8.8.8 ]]; then
cat > "${BUILD_PATH}/build/config.json" << EOL
{
"remoteDnsServers": [[$(echo "$DNS" | tr . ,)],[8,8,8,8]],
"envs": [
	{
		"name": ""
	}
],
"activeEnv": "",
"lastId": 0,
"webServerPort": 0,
"dnsServerPort": 0,
"defaultDns": null,
"logLevel": "",
"logFile": "",
"registerContainerNames": null
}
EOL
fi



# Generate concourse.env environment file with user details

cat > "${BUILD_PATH}/build/concourse.env" << EOL
# concourse-web
CONCOURSE_EXTERNAL_URL=https://${EXTERNAL_URL_CONCOURSE}:443
CONCOURSE_POSTGRES_HOST=db
CONCOURSE_POSTGRES_USER=$USER_NAME
CONCOURSE_POSTGRES_PASSWORD=$PASSWORD
CONCOURSE_POSTGRES_DATABASE=$DATABASE
CONCOURSE_POSTGRES_PORT=5432
CONCOURSE_MAIN_TEAM_LOCAL_USER=$USER_NAME
CONCOURSE_ADD_LOCAL_USER=$USER_NAME:$PASSWORD
CONCOURSE_SESSION_SIGNING_KEY=${BUILD_PATH}/keys/session_signing_key
CONCOURSE_TSA_HOST_KEY=${BUILD_PATH}/keys/tsa_host_key
CONCOURSE_TSA_AUTHORIZED_KEYS=${BUILD_PATH}/keys/authorized_worker_keys
CONCOURSE_TLS_BIND_PORT=443
CONCOURSE_TLS_CERT=${BUILD_PATH}/certs/$COMMON_NAME_CONCOURSE.crt
CONCOURSE_TLS_KEY=${BUILD_PATH}/certs/$COMMON_NAME_CONCOURSE.key

# concourse-worker
CONCOURSE_GARDEN_DNS_PROXY_ENABLE=true
CONCOURSE_GARDEN_DNS_SERVER=$DNS
CONCOURSE_WORK_DIR=${BUILD_PATH}/concourse/worker
CONCOURSE_TSA_WORKER_PRIVATE_KEY=${BUILD_PATH}/keys/worker_key
CONCOURSE_TSA_PUBLIC_KEY=${BUILD_PATH}/keys/tsa_host_key.pub
CONCOURSE_TSA_HOST=ci:2222

# concourse-db
POSTGRES_DB=$DATABASE
POSTGRES_PASSWORD=$PASSWORD
POSTGRES_USER=$USER_NAME

# gitlab
GITLAB_ROOT_PASSWORD=$PASSWORD

# path to keys
KEYS_PATH=$BUILD_PATH/keys

# registry
REGISTRY_HTTP_SECRET=$(head -c 9 /dev/urandom | openssl base64 -e)
REGISTRY_HTTP_TLS_CERTIFICATE=${BUILD_PATH}/certs/$COMMON_NAME_REGISTRY.crt
REGISTRY_HTTP_TLS_KEY=${BUILD_PATH}/certs/$COMMON_NAME_REGISTRY.key
REGISTRY_AUTH=htpasswd
REGISTRY_AUTH_HTPASSWD_PATH=${BUILD_PATH}/auth/htpasswd
REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm

EOL


# Generate .env environment file for use of docker-compose.yml

cat > "${BUILD_PATH}/build/.env" << EOL

# Image tags
POSTGRES=11.4
CI=${CONCOURSE_VERSION}
GIT=${GITLAB_VERSION}

# Volume paths
BUILD=${BUILD_PATH}/build
KEYS=${BUILD_PATH}/keys
CERTS=${BUILD_PATH}/certs
PIPELINES=${BUILD_PATH}/pipelines
CONCOURSE=${BUILD_PATH}/concourse
GITLAB=${BUILD_PATH}/gitlab
REGISTRY=${BUILD_PATH}

# 5.7.0
CONCOURSE_WORKER_WORK_DIR=${BUILD_PATH}/concourse/worker
CONCOURSE_TSA_BIND_IP=worker
CONCOURSE_TSA_BIND_PORT=2222
EOL

# Copy the docker-compose.yml, setup-private-registry.sh and setup-pipeline.sh scripts to build directory

cp ./docker-compose.yml ./setup-private-registry.sh ./setup-pipeline.sh "${BUILD_PATH}/build"

# Generate certificates

if [ -z "$CC_TLS_PUBKEY" ]; then
    echo -e "$CHECK Creating certificates"

    openssl req \
            -newkey rsa:4096 \
            -new \
            -nodes \
            -x509 \
            -days 365 \
            -subj '/CN='${COMMON_NAME_CONCOURSE}'/O=CinCan/C=FI' \
            -extensions san \
            -config <(echo "[req]"; echo distinguished_name=req; echo "[san]"; echo subjectAltName=DNS:"${ALT_EXTERNAL_URL_CONCOURSE}",IP:"${EXTERNAL_URL_CONCOURSE}") \
            -sha256 \
            -keyout "${BUILD_PATH}/certs/${COMMON_NAME_CONCOURSE}.key" \
            -out "${BUILD_PATH}/certs/${COMMON_NAME_CONCOURSE}.crt" > /dev/null 2>&1

    openssl req \
            -newkey rsa:4096 \
            -new \
            -nodes \
            -x509 \
            -days 365 \
            -subj '/CN='${COMMON_NAME_GITLAB}'/O=CinCan/C=FI' \
            -extensions san \
            -config <(echo "[req]"; echo distinguished_name=req; echo "[san]"; echo subjectAltName=DNS:"${ALT_EXTERNAL_URL_GITLAB}",IP:"${EXTERNAL_URL_GITLAB}") \
            -sha256 \
            -keyout "${BUILD_PATH}/certs/${COMMON_NAME_GITLAB}.key" \
            -out "${BUILD_PATH}/certs/${COMMON_NAME_GITLAB}.crt" > /dev/null 2>&1

    openssl req \
            -newkey rsa:4096 \
            -new \
            -nodes \
            -x509 \
            -days 365 \
            -subj '/CN='${COMMON_NAME_REGISTRY}'/O=CinCan/C=FI' \
            -extensions san \
            -config <(echo "[req]"; echo distinguished_name=req; echo "[san]"; echo subjectAltName=DNS:"${ALT_EXTERNAL_URL_REGISTRY}",IP:"${EXTERNAL_URL_REGISTRY}") \
            -sha256 \
            -keyout "${BUILD_PATH}/certs/${COMMON_NAME_REGISTRY}.key" \
            -out "${BUILD_PATH}/certs/${COMMON_NAME_REGISTRY}.crt" > /dev/null 2>&1


else # premade cert paths passed in env vars
    if [[ -f "$CC_TLS_PUBKEY" && -f "$CC_TLS_PRIVKEY" ]]; then
	cp "$CC_TLS_PUBKEY" "${BUILD_PATH}/certs/${COMMON_NAME_CONCOURSE}.crt"
	cp "$CC_TLS_PRIVKEY" "${BUILD_PATH}/certs/${COMMON_NAME_CONCOURSE}.key"
	cp "$CC_TLS_PUBKEY" "${BUILD_PATH}/certs/${COMMON_NAME_GITLAB}.crt"
	cp "$CC_TLS_PRIVKEY" "${BUILD_PATH}/certs/${COMMON_NAME_GITLAB}.key"
	cp "$CC_TLS_PUBKEY" "${BUILD_PATH}/certs/${COMMON_NAME_REGISTRY}.crt"
	cp "$CC_TLS_PRIVKEY" "${BUILD_PATH}/certs/${COMMON_NAME_REGISTRY}.key"

    else
	echo "CC_TLS_PUBKEY env var provided but one of the pubkey/privkey files is missing"
	exit 1
    fi
fi

# Create gitlab configuration file

cat > "${BUILD_PATH}/gitlab/config/gitlab.rb" <<EOF
# Full list of available configuration options can be found from:
# https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template

external_url 'https://${EXTERNAL_URL_GITLAB}:5443';
gitlab_rails['lfs_enabled'] = true;
letsencrypt['enable'] = false;
nginx['ssl_certificate'] = '/etc/ssl/certs/gitlab/$COMMON_NAME_GITLAB.crt';
nginx['ssl_certificate_key'] = '/etc/ssl/certs/gitlab/$COMMON_NAME_GITLAB.key';
EOF


## Create certs and credentials for private registry

echo -e "$CHECK Added certificate for Docker private registry" "${BUILD_PATH}"/certs/"${COMMON_NAME_REGISTRY}".crt /etc/docker/certs.d/"${EXTERNAL_URL_REGISTRY}":5000/ca.crt
if [ ! -d /etc/docker/certs.d/"${EXTERNAL_URL_REGISTRY}":5000 ]; then mkdir -p /etc/docker/certs.d/"${EXTERNAL_URL_REGISTRY}":5000; fi
cp "${BUILD_PATH}"/certs/"${COMMON_NAME_REGISTRY}".crt /etc/docker/certs.d/"${EXTERNAL_URL_REGISTRY}":5000/ca.crt

if [ ! -d "${BUILD_PATH}"/auth ]; then mkdir "${BUILD_PATH}"/auth; fi
if htpasswd -Bbc "${BUILD_PATH}"/auth/htpasswd "${USER_NAME}" "${PASSWORD}"; then
	echo -e "$CHECK passwd file created for private registry"
else
	echo -e "$ERROR Htpasswd failed, is it installed? (Install: apache2-utils for Debian based or httpd-tools for RedHat derived OS)"
	exit 1
fi


## Create README and inform user

cat > "${BUILD_PATH}/build/README" << EOL
Now you should be able to build the pilot environment using following commands on host system:

1. cd ${BUILD_PATH}/build
2. docker-compose up -d db ci worker gitlab

.. or if you want to use dns-proxy

2. docker-compose up -d db ci worker gitlab dns

.. or with Docker private registry

2. docker-compose up -d db ci worker gitlab reg
3. sudo bash ./setup-private-registry.sh


Use and login to the services on the pilot environment with following credentials:
* username: $USER_NAME
* password: $PASSWORD

Local Gitlab credentials:
* username: root
* password: $PASSWORD

Services:
* Concourse CI: https://${EXTERNAL_URL_CONCOURSE}:443 or https://${ALT_EXTERNAL_URL_CONCOURSE} (if proxy is enabled)
* CinCan Gitlab: https://${EXTERNAL_URL_GITLAB}:5443 or https://${ALT_EXTERNAL_URL_GITLAB} (if proxy is enabled)
* Private registry https://${EXTERNAL_URL_REGISTRY}:5000 (if enabled)
EOL

echo -e "$CHECK cat ${BUILD_PATH}/build/README"
echo ""
cat "${BUILD_PATH}/build/README"
echo ""


