#!/bin/bash

# Variable definitions

CHECK="\e[32m[+]\e[0m"
PRIVATE_REGISTRY=172.20.0.6

# Whiptail colors

export NEWT_COLORS='
backtitle=,red
root=,gray
border=black,lightgray
window=,lightgray
shadow=gray,black
title=red,lightgray
checkbox=white,gray
actcheckbox=green,red
button=lightgray,red
actbutton=red,lightgray'


# Check if user is root

if [ ! "${UID}" -eq 0 ] ;then
        echo "Please run the script as root"; exit 0
fi


# Welcome banner
echo "    _______       ______           "
echo "  / ____(_)___  / ____/___ _____   "
echo " / /   / / __ \/ /   / __ \`/ __ \ "
echo "/ /___/ / / / / /___/ /_/ / / / /  "
echo "\____/_/_/ /_/\____/\__,_/_/ /_/   "
echo ""
echo "Continuous Integration for the Collaborative Analysis of Incidents"
echo ""
echo "* Private registry setup *"
echo ""


# Functions
# Get/ask for Docker username (because root is not necessarily the Docker user), cincan credentials, and login to registry

function getCredentials() {

	# Get docker user's name from /etc/group (tries the last user mentioned in docker group)
	if [ ! "$DOCKER_USERNAME" ]; then
		DOCKER_USERNAME=$(cat /etc/group |grep docker: |tail -1 | awk -F "," '{print $NF }' |awk -F ":" '{print $NF }')
	fi
	echo "Login to private registry:"
	sudo su "$DOCKER_USERNAME" -c "docker login 172.20.0.6:5000" || { echo "Could not resolve docker user."; \
		echo "Type in your username in docker group (q to quit): "; read -r DOCKER_USERNAME; \
		if [ "$DOCKER_USERNAME" -eq 'q' ];then exit 0;else getCredentials; fi }
}


# Clone tools repository (or update if exists)

function cloneRepo() {
	echo -e "$CHECK Cloning the tools repository"
	if [ ! -d "tools" ];then
		git clone https://gitlab.com/CinCan/tools.git
		cd tools || exit
	else
		cd tools || exit
		git pull
	fi
}


# User menu

# Create image selection options

function createMenuOptions() {
	TOOLS_LIST=$(ls -d -- */)
	TOOLS_COUNT=$(echo "$TOOLS_LIST" |grep -o "/" |wc -l)

	i=1; while
	        OPTION="$(echo $TOOLS_LIST |cut -d " " -f $i)"
	        OPTION="${OPTION::-1}"

	        if [ -f "$OPTION/Dockerfile" ]; then
	                if cat "$OPTION/Dockerfile" |head -n 10 |grep -q microsoft; then
	                        OS="Windows"
	                else
	                        OS="Linux"
	                fi

	                OPTIONS="${OPTIONS} ${OPTION} ${OS} OFF"
	        fi

	        i=$((i+1))
        	[ $i -lt $((TOOLS_COUNT+1)) ]
	do :;done
}

# Menu to select images

function selectImages() {
	TOOLS_TO_INSTALL=$(whiptail --title "Cincan private registry setup" --checklist --fb --backtitle \
		"  CinCan  -  Continuous Integration for the Collaborative Analysis of Incidents  " \
		"There are $TOOLS_COUNT images available. Choose the images to install:" \
		30 80 20 $OPTIONS 3>&1 1>&2 2>&3)

	if [ ! "$TOOLS_TO_INSTALL" ]; then echo "Cancelled"; exit; fi

	TOOLS_TO_INSTALL="${TOOLS_TO_INSTALL//\"}"
}


# Menu for selecting installation method

function installationMethod() {
	INSTALLATION_METHOD=$(whiptail --title "Cincan private registry setup" --fb --menu "Choose installation method" --backtitle \
		" CinCan  -  Continuous Integration for the Collaborative Analysis of Incidents " \
		15 60 4 \
	        "1" "Build images from dockerfiles" \
       	 	"2" "Download images from hub.docker.com" \
	        "3" " ⇦ RETURN TO IMAGE SELECTION MENU" 3>&1 1>&2 2>&3)

	case "$INSTALLATION_METHOD" in
      	"")
		echo Cancelled; exit 0
	;;
      	1)
        	echo "Building images from the dockerfiles"
        ;;
        2)
		echo "Downloading selected images from hub.docker.com"
        ;;
        3)
		# Set previous selections to be selected also when returning to menu
		i=1; while
		        TOOL_NUMBER="\$"$i
		        TOOL_ON=$(echo "$TOOLS_TO_INSTALL" |awk "{print $TOOL_NUMBER}")
                        TOOL_OS=$(echo "$OPTIONS" |grep -Po -- "$TOOL_ON \K\w*")
                        OPTIONS="${OPTIONS//$TOOL_ON $TOOL_OS OFF/$TOOL_ON $TOOL_OS ON}"
			i=$((i+1))
		        [ $i -lt $((TOOLS_COUNT+2)) ]
		do :;done

        	selectImages
		installationMethod
        ;;
	esac
}


# Build the images

function buildImages(){
	echo -e "$CHECK Building Dockerfiles..."

	i=1
	while
		CURRENT_TOOL="$(echo $TOOLS_TO_INSTALL |cut -d " " -f $i)"
		echo -e "\n$CHECK Building tool $i/$TOOLS_COUNT_TO_INSTALL: $CURRENT_TOOL\n"
		cd "$CURRENT_TOOL" || exit

		echo "$CURRENT_TOOL" >> times
		sudo su "$DOCKER_USERNAME" -c "docker build . -t cincan/$CURRENT_TOOL"

		cd ..
	        i=$((i+1))
	        [ $i -lt $((TOOLS_COUNT_TO_INSTALL+1)) ]
	do :;done
}


# Pull images

function pullImages(){
	echo -e "$CHECK Downloading images from Docker hub"

	i=1
	while
		CURRENT_TOOL=$(echo "$TOOLS_TO_INSTALL" |cut -d " " -f $i)
		echo -e "\n$CHECK Downloading tool $i/$TOOLS_COUNT_TO_INSTALL: $CURRENT_TOOL\n"
		cd "$CURRENT_TOOL" || exit

		echo "$CURRENT_TOOL" >> times
		docker pull cincan/"$CURRENT_TOOL"

		cd ..
	        i=$((i+1))
	        [ $i -lt $((TOOLS_COUNT_TO_INSTALL+1)) ]
	do :;done
}


# Push images to the private registry

function pushImages() {
	echo -e "$CHECK Pushing images to Docker private registry"

	i=1
	while
		CURRENT_TOOL=$(echo "$TOOLS_TO_INSTALL" |cut -d " " -f $i)
		docker tag cincan/"$CURRENT_TOOL" "$PRIVATE_REGISTRY:5000/$CURRENT_TOOL"
		echo -e "$CHECK tagged cincan/$CURRENT_TOOL as $PRIVATE_REGISTRY:5000/$CURRENT_TOOL"

		sudo su "$DOCKER_USERNAME" -c "docker push $PRIVATE_REGISTRY:5000/$CURRENT_TOOL"
		echo -e "$CHECK pushed $CURRENT_TOOL to $PRIVATE_REGISTRY:5000/"

		docker rmi -f cincan/"$CURRENT_TOOL"
		docker rmi -f "$PRIVATE_REGISTRY:5000/$CURRENT_TOOL"
		echo -e "$CHECK removed $CURRENT_TOOL from host"

	        i=$((i+1))
	        [ $i -lt $((TOOLS_COUNT_TO_INSTALL+1)) ]
	do :;done
}


# Execute functions

getCredentials
cloneRepo
createMenuOptions
selectImages
installationMethod

TOOLS_COUNT_TO_INSTALL=$(echo "$TOOLS_TO_INSTALL" |wc -w)

if [ "$INSTALLATION_METHOD" = 1 ]; then
	buildImages
else
	pullImages
fi

pushImages

echo -e "\n\n$CHECK \e[32mPrivate registry is ready.\e[0m"
echo -e "\nTo get the list of images in the private registry, type:"
echo "curl -X GET https://<USERNAME>:<PASSWORD>@172.20.0.6:5000/v2/_catalog --insecure"

