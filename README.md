[![pipeline status](https://gitlab.com/CinCan/environment/badges/master/pipeline.svg)](https://gitlab.com/CinCan/environment/pipelines)

# Introduction

This repository contains the CinCan pilot environment that constructs from two main services [Concourse CI](https://concourse-ci.org/) and 
[Gitlab CE](https://gitlab.com/gitlab-org/gitlab-ce/). Concourse CI is used for creating, executing and managing the [CinCan CI pipelines](https://gitlab.com/CinCan/pipelines) whereas 
Gitlab CE stores samples to be analyzed, found artifacts, results, logs etc. Every component of the service is run in a separate Docker container 
which makes the environment easy to deploy and destroy without affecting the host system.

The full list of components:  

* Concourse CI  
    * [Web node](https://concourse-ci.org/concourse-web.html)  
    * [Worker node](https://concourse-ci.org/concourse-worker.html)  
    * [PostgreSQL node](https://concourse-ci.org/postgresql-node.html)  
* Gitlab CE  
* [dns-proxy-server](https://github.com/mageddo/dns-proxy-server) (optional)
* Docker Registry 2.0 (optional private registry)
* [add2git-lfs](https://gitlab.com/saguywalker/add2git-lfs) (experimental)

# Prerequisites

* Ubuntu 18.04 (others untested)
* Ensure that your system has more than 6 GB memory
* Ensure that your system has more than 4 GB free disk space  
* [Docker](https://docs.docker.com/install/)  >= 17.12.0
* [Docker Compose](https://docs.docker.com/compose/install/) >= 1.18.0


# 1. Install docker & docker-compose

[Read the wiki for help about installing/upgrading docker & docker-compose.](https://gitlab.com/CinCan/environment/wikis/Installing-up-to-date-docker)  


# 2. Deploy the pilot environment

```bash
# Clone the repository
git clone https://gitlab.com/CinCan/environment.git

# Change directory
cd ./environment

# Run the script (requires root privileges)
sudo bash build.sh -e BUILD_PATH=/path/to (default: /opt/cincan)

# Change directory
cd /path/to/build

# Build the environment using Docker Compose
docker-compose up -d db \
                     ci \
                     worker \
                     gitlab \
                     dns (optional) \
                     reg (optional)
```

The Gitlab will take some time to get up (~5 minutes).
You can follow the starting process using following command:

```bash
docker logs gitlab.cincan.io --follow
```  

[* See an asciicast video of the environment setup *](https://asciinema.org/a/cHrci9nG1HbtszMsm3hrnwdn4)  



# 3. Set up a pipeline

You can list all pipelines that have a setup script with ```sudo bash setup-pipeline.sh```

```bash
sudo bash setup-pipeline.sh  

[+] Cloning the pipelines.git

Available pipelines
1) pdf-pipeline
2) pdf-pipeline Private registry version
3) document-pipeline
4) pe-pipeline
5) Quit
```

Pipelines currently with the quick setup: [pdf-pipeline](https://gitlab.com/CinCan/pipelines/tree/master/pdf-pipeline), 
the more advanced [document-pipeline](https://gitlab.com/CinCan/pipelines/tree/master/document-pipeline), and a [pe-pipeline](https://gitlab.com/CinCan/pipelines/tree/master/pe-pipeline)

To setup a pipeline:

1. Set up [the pilot environment](https://gitlab.com/CinCan/environment)

2. sudo bash setup-pipeline.sh

3. Select the pipeline to set up

4. Login to https://172.20.0.3 to see the pipeline work. (Or https://concourse.cincan.io if proxy is enabled)


[* See an asciicast video of the pipeline setup *](https://asciinema.org/a/cHrci9nG1HbtszMsm3hrnwdn4?t=1:35)


---


## How to stop and remove the whole pilot environment

```bash
cd /path/to/build
docker-compose down --volumes --rmi all && rm -rf /path/to
```


## Steps to set up a pipeline in the private registry environment

1. Deploy the pilot environment as described above, including the "reg" option  

2. Run ```sudo bash setup-private-registry.sh```  

```bash
sudo bash setup-private-registry.sh
```  

* Type in environment username and password when prompted.  
* Select the tools to use from the pop up dialog box. For example, if you are setting up the pdf-pipeline, check "jsunpack-n", "pdfid" and "peepdf".  
* Choose whether you want to download the tool images from the Docker hub, or to build them locally.  

3. Run ```sudo bash setup-pipeline.sh``` and choose a Private registry version pipeline  

4. Login to https://172.20.0.3 to see the pipeline work. (Or https://concourse.cincan.io if proxy is enabled)



## Read more

Read more about [the pilot environment installation options from the wiki](https://gitlab.com/CinCan/environment/wikis/home)  
Read more about [our pipelines](https://gitlab.com/CinCan/pipelines/)  

