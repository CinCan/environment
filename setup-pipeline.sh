#!/bin/bash

# Welcome banner
echo "    _______       ______           "
echo "  / ____(_)___  / ____/___ _____   "
echo " / /   / / __ \/ /   / __ \`/ __ \ "
echo "/ /___/ / / / / /___/ /_/ / / / /  "
echo "\____/_/_/ /_/\____/\__,_/_/ /_/   "
echo ""
echo "Continuous Integration for the Collaborative Analysis of Incidents"
echo ""

CHECK="\e[32m[+]\e[0m"
ERROR="\e[0;91m[x]\e[0m"
PIPELINE=$1	# Pipeline name can be set as first argument
TOKEN=$2 	# You can set a previously created token as the second argument


# Use current path
BUILD_PATH=${PWD%/build*}


# Check if user is root
if [ ! "${UID}" -eq 0 ] ;then echo "Please run the script as root"; exit 0; fi


# Clone pipelines.git to local machine
echo -e "$CHECK Cloning the pipelines.git"
[ ! -d "pipelines" ] && git clone https://gitlab.com/CinCan/pipelines.git || echo "Folder already exists, updating..." && cd pipelines && git pull && cd ..


# Get available pipelines
PIPELINES=$(find pipelines/ \( -name "setup.json" \) | grep -oP './\K.*?(?=/)')


# Help, list available pipelines
[ "$1" = "-h" ] || [ "$1" = "--help" ] &&
        echo -e "Usage: sudo ./setup-pipeline.sh <PIPELINE>" &&
        echo -e "\nAvailable pipelines: \n$PIPELINES" &&
	exit 0


# Pipeline selection menu
if [ "$1" == "" ]; then
        # Get pipelines with setup.sh, and possible private registry versions
        echo -e "\nAvailable pipelines"

        i=0 && j=0
        while [ "$i" -lt "$(echo "$PIPELINES" | wc -l)" ]; do
                i=$((i+1)) && j=$((j+1))
                OPTIONS[j]=$(echo "$PIPELINES" |awk NR==$i)
                if [ -f pipelines/${OPTIONS[$j]}/pipeline-private-registry.yml ]; then
                        j=$((j+1))
                        REGISTRY_VERSION="Private registry version"
                        OPTIONS[j]=$(echo "$PIPELINES" |awk NR=="$i")" $REGISTRY_VERSION"
                fi
        done

        # Add quit option
        OPTIONS[$((j+1))]="Quit"

        # Menu
        PS3="Your choice: "
        select CHOSEN_PIPELINE in "${OPTIONS[@]}"; do
                if [[ -n "$CHOSEN_PIPELINE" ]]; then
                        break
                else
                        if [ "$REPLY" = "q" ] || [ "$REPLY" = "Q" ]; then exit; fi
                        echo -e "$ERROR invalid option"
                fi
        done
fi


# Check if private registry was chosen
if [[ "${CHOSEN_PIPELINE}" == *"Private"* ]]; then
        PIPELINE="${CHOSEN_PIPELINE/" $REGISTRY_VERSION"/}"
else
	if ! [ $1 ]; then PIPELINE="${CHOSEN_PIPELINE}"; fi
	REGISTRY_VERSION=""
fi


# Change directory and launch pipeline setup.sh, if exists
[ "$PIPELINE" != "Quit" ] &&
        cd pipelines || exit &&
        [ -d "$PIPELINE" ] && ( echo -e "$CHECK Run setup.sh -e $BUILD_PATH -p $PIPELINE" \
                && [ -f "./setup.sh" ] && ./setup.sh -e "$BUILD_PATH" -t "$TOKEN" -p "$PIPELINE" "$REGISTRY_VERSION" \
                || echo -e "$ERROR ...script interrupted..." ) || echo -e "$ERROR Pipeline $PIPELINE not found"

